/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import modelo.Contratista;

/**
 *
 * @author Juans
 */
@Stateless
public class ContratistaFacade extends AbstractFacade<Contratista> implements ContratistaFacadeLocal {

    @PersistenceContext(unitName = "ControlContratosPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public ContratistaFacade() {
        super(Contratista.class);
    }
    
    @Override
    public Contratista findxNit(Long nit) {
        String consulta = "SELECT c FROM Contratista c WHERE c.nitcontratista = " + nit;
        try {
            Query query = em.createQuery(consulta);
            return (Contratista) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }
 
    @Override
    public Contratista findxCod(Integer cod) {
        String consulta = "SELECT c FROM Contratista c WHERE c.codigocontratista = " + cod;
        try {
            Query query = em.createQuery(consulta);
            return (Contratista) query.getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

}
