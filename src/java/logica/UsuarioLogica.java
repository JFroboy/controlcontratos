/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import modelo.Usuario;
import persistencia.UsuarioFacadeLocal;

/**
 *
 * @author Juans
 */
@Stateless
public class UsuarioLogica implements UsuarioLogicaLocal {

    @EJB
    private UsuarioFacadeLocal usuarioDAO;

    @Override
    public Usuario ingresar(Usuario u) throws Exception {
        if (u == null) {
            throw new Exception("Usuario vacio");
        }

        if (u.getNombreusuario().equals("")) {
            throw new Exception("Nombre de usuario obligatorio");
        }

        if (u.getClaveusuario().equals("")) {
            throw new Exception("Clave obligatoria");
        }

        Usuario usuarioLogin = usuarioDAO.consultarLogin(u.getNombreusuario(), u.getClaveusuario());

        if (usuarioLogin == null) {
            throw new Exception("Usuario o clave invalido");
        }
        return usuarioLogin;
    }
}
