 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.util.List;
import javax.ejb.Local;
import modelo.Contratista;

/**
 *
 * @author Juans
 */
@Local
public interface ContratistaLogicaLocal {
    
    public List<Contratista> listaContratistas();
    
    public void registrarContratista(Contratista c) throws Exception;
    
    public void actualizarContratista(Contratista c) throws Exception;
    
    public Contratista consultarxNit(Long nit);
    
    public Contratista consultarxCod(Integer cod);
    
    public void eliminarContratista(Contratista c) throws Exception;
    
    public String importarDatosContratistas(String archivo) throws Exception;
    
}
