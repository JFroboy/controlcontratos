/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import java.io.File;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import jxl.Sheet;
import jxl.Workbook;
import modelo.Contratista;
import persistencia.ContratistaFacadeLocal;

/**
 *
 * @author Juans
 */
@Stateless
public class ContratistaLogica implements ContratistaLogicaLocal {
    
    /*  El objeto permite utilizar los metodos de la Clase ContratistaFacade (persistencia), 
    este objeto siempre debe ser definido por la interfaz de la clase*/
    @EJB
    private ContratistaFacadeLocal contratistaDAO;
    
    /* Retorna una lista con los contratistas almacenados */
    @Override
    public List<Contratista> listaContratistas() {
        
        /*la consulta query se realiza utilizando uno de los metodos predeterminados findAll()*/
        return contratistaDAO.findAll();
    }
    
    /* Retorna una objeto tipo contratista dado un nit*/ 
    @Override
    public Contratista consultarxNit(Long nit) {
        /*Se ejecuta el metodo de la clase ContratistaFacade definido (findxNit)*/
        return contratistaDAO.findxNit(nit);
    }
    
     /* Retorna una objeto tipo contratista dado un codigo*/
    @Override
    public Contratista consultarxCod(Integer cod) {
        /*Se ejecuta el metodo de la clase ContratistaFacade definido (findxCod)*/
        return contratistaDAO.findxCod(cod);
    }
    
    /* Metodo para registrar un contratista en la Base de Datos */
    @Override
    public void registrarContratista(Contratista c) throws Exception {
        
        /* Se valida que la información digita sea la correcta para poder 
        almacedar el contratista en la BD*/
        
        if (c == null) {
            throw new Exception("El Contratista no tiene información");
        }

        if (c.getNitcontratista() == 0) {
            throw new Exception("El NIT es obligatorio");
        }

        if (c.getNombrecontratista().equals("")) {
            throw new Exception("El Nombre es obligatorio");
        }
        
        /* ============================================== */
        
        /* Se verifica que ningun contratista existente tenga el mismo NIT digirado */
        Contratista contratista = consultarxNit(c.getNitcontratista());

        if (contratista != null) {
            throw new Exception("El Contratista:" + c.getNitcontratista() + " ya existe");
        }
        
        /* create es un metodo por defecto de la clase ContratistaFacade */
        contratistaDAO.create(c);
    }
    
    /* Metodo para actualizar los datos de un contratista */
    @Override
    public void actualizarContratista(Contratista c) throws Exception {
        
        /* Se valida que el contratista que se va actualizar exista */
        if (null == c) {
            throw new Exception("El Contratista no tiene información");
        }
        
        if(consultarxCod(c.getCodigocontratista())==null){
            throw new Exception("El contratista no existe");
        }
        
        /* edit es un metodo por defecto de la clase ContratistaFacade */
        contratistaDAO.edit(c);
    }
    
    /* Metodo para eliminar un contratista */
    @Override
    public void eliminarContratista(Contratista c) throws Exception {
        
        if(c == null){
            throw new Exception("El Contratista no tiene información");           
        }
        
         Contratista objBorrar = consultarxCod(c.getCodigocontratista());
        
         /* Se valida que el contratista exista */
        if (objBorrar == null) {
            throw new Exception("El Contratista no existe");
        }

        /* Validar que el contratista no tenga ingresos ni contratos */ 
        if (objBorrar.getContratosList().size() > 0) {
            throw new Exception("El Contratista tiene contratos asociados");
        }

        if (objBorrar.getIngresoList().size() > 0) {
            throw new Exception("El Contratista tiene ingresos asociados");
        }
        
        /* remove es un metodo por defecto de la clase ContratistaFacade  */
        contratistaDAO.remove(objBorrar);
    }
    
    /* Metodo para leer un archivo .xls y almacenar en la base de datos los
    contratistas registrados en el documento*/
    @Override
    public String importarDatosContratistas(String archivo) throws Exception {
        
        /*Contratista registrado*/
        int contratistasR = 0;
        /*Contratista existente*/
        int contratistasE = 0;
        
        /* se crea un objecto de tipo WorkBook para almacenar el archivo seleccionado */
        Workbook archivoExcel = Workbook.getWorkbook(new File(archivo));
        
        /* Se selecciona la primer hoja del archivo de excel */
        Sheet hoja = archivoExcel.getSheet(0);
        
        int filas = hoja.getRows();
        for (int i = 1; i < filas; i++) {
            Contratista nuevoContratista = new Contratista();
            nuevoContratista.setNitcontratista(
                    Long.parseLong(hoja.getCell(0, i).getContents()));
            nuevoContratista.setNombrecontratista(hoja.getCell(1, i).getContents());
            nuevoContratista.setEstadocontratista("ACTIVO");
            
            /*Se valida que el contratista no esté almacenado en la base de datos
            para poder ser registrado*/
            Contratista objC = consultarxNit(nuevoContratista.getNitcontratista());
            if(objC == null){
                contratistaDAO.create(nuevoContratista);
                contratistasR++;
            }
            else{
                contratistasE++;
            }
            
        }
        return "Se registraron: " + contratistasR + " Contratistas y ya existian: " + contratistasE;
    }
}
