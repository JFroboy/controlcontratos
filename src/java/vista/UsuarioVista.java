/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import logica.UsuarioLogicaLocal;
import modelo.Usuario;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.password.Password;

/**
 *
 * @author Juans
 */
@Named(value = "usuarioVista")
@ManagedBean
@RequestScoped
public class UsuarioVista {

    /**
     * Creates a new instance of UsuarioVista
     */
    public UsuarioVista() {
    }
    
    @EJB
    private UsuarioLogicaLocal usuarioLogica;

    private InputText txtUsuario;
    private Password txtPassword;
    private CommandButton btnIngresar;
    
    public InputText getTxtUsuario() {
        return txtUsuario;
    }

    public Password getTxtPassword() {
        return txtPassword;
    }

    public CommandButton getBtnIngresar() {
        return btnIngresar;
    }

    public void setTxtUsuario(InputText txtUsuario) {
        this.txtUsuario = txtUsuario;
    }

    public void setTxtPassword(Password txtPassword) {
        this.txtPassword = txtPassword;
    }

    public void setBtnIngresar(CommandButton btnIngresar) {
        this.btnIngresar = btnIngresar;
    }

    public void ingresar() {

        try {
            String usuario = txtUsuario.getValue().toString();
            String password = txtPassword.getValue().toString();

            Usuario nuevoUsuario = new Usuario();
            nuevoUsuario.setNombreusuario(usuario);
            nuevoUsuario.setClaveusuario(password);

            Usuario usuarioLogeado = usuarioLogica.ingresar(nuevoUsuario);
            // Guardar usuario en la sesion
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().put("usuario", usuarioLogeado);
            // Redireccionar a la página
            FacesContext.getCurrentInstance().getExternalContext().redirect("home.xhtml");
            
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_ERROR,"Error al iniciar sesion: ", ex.getMessage())); 
        }

    }

    public void cerrarSesion() {
        try {
            FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("usuario");
            FacesContext.getCurrentInstance().getExternalContext().redirect("index.xhtml");
        } catch (IOException ex) {
            Logger.getLogger(UsuarioVista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
