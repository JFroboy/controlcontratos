/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import logica.ContratistaLogicaLocal;
import modelo.Contratista;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import org.primefaces.component.commandbutton.CommandButton;
import org.primefaces.component.inputtext.InputText;
import org.primefaces.component.selectonemenu.SelectOneMenu;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Juans
 */
@Named(value = "contratistaVista")
@RequestScoped
public class ContratistaVista {

    /**
     * Creates a new instance of ContratistaVista
     */
    public ContratistaVista() {
    }
    
    /** Objeto tipo ContratistaLogicaLocal para utilizar los metodos
        de la clase LogicaContratista*/
    @EJB
    private ContratistaLogicaLocal contratistaLogica;
    
    /** Lista para almacenar los contratistas de determinadas consultas*/
    private List<Contratista> listaContratistas;

    private InputText txtNitContratista;
    private InputText txtNombreContratista;
    private SelectOneMenu cmbEstadoContratista;
    private CommandButton btnRegistrar;
    private CommandButton btnEliminar;
    private CommandButton btnModificar;
    
    /** Objeto contratista que toma los valores de un contratista seleccionado en 
        la tabla y actualiza los campos del formulario*/
    private Contratista selectContratista;
    
    /** Metodo que actualiza la listaContratista con los datos de la Base de datos 
        de la tabla contratista.
        Se utiliza el metodo de la clase LogicaContratist
     * @return listaContratistas **/
    public List<Contratista> getListaContratistas() {
        listaContratistas = contratistaLogica.listaContratistas();
        return listaContratistas;
    }

    public void setListaContratistas(List<Contratista> listaContratistas) {
        this.listaContratistas = listaContratistas;
    }

    public InputText getTxtNitContratista() {
        return txtNitContratista;
    }

    public void setTxtNitContratista(InputText txtNitContratista) {
        this.txtNitContratista = txtNitContratista;
    }

    public InputText getTxtNombreContratista() {
        return txtNombreContratista;
    }

    public void setTxtNombreContratista(InputText txtNombreContratista) {
        this.txtNombreContratista = txtNombreContratista;
    }

    public SelectOneMenu getCmbEstadoContratista() {
        return cmbEstadoContratista;
    }

    public void setCmbEstadoContratista(SelectOneMenu cmbEstadoContratista) {
        this.cmbEstadoContratista = cmbEstadoContratista;
    }

    public Contratista getSelectContratista() {
        return selectContratista;
    }

    public void setSelectContratista(Contratista selectContratista) {
        this.selectContratista = selectContratista;
    }

    public void limpiarCampos() {
        txtNitContratista.setValue("");
        txtNombreContratista.setValue("");
    }

    /** Metodo para registrar un contratista 
     Los datos se obtienen de los campos del formulario y se crea un obj tipo 
     Contratista**/
    public void registrarContratista() {
        try {
            Contratista nuevoContratista = new Contratista();
            nuevoContratista.setNitcontratista(Long.parseLong(txtNitContratista.getValue().toString()));
            nuevoContratista.setNombrecontratista(txtNombreContratista.getValue().toString());
            nuevoContratista.setEstadocontratista(cmbEstadoContratista.getValue().toString());

            contratistaLogica.registrarContratista(nuevoContratista);
            
            /* Se actualiza el mensaje de información */
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "mensaje", "El Contratista ha sido registrado"));
            limpiarCampos();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "mensaje", ex.getMessage()));
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /** Metodo que almacena los datos del contratista seleccionado en la tabla.
     La tabla tiene una propiedad que activa el evento y el objeto selecContratista 
     es actualizado**/
    public void seleccionarContratista(SelectEvent e) {
        selectContratista = (Contratista) e.getObject();
        txtNitContratista.setValue(selectContratista.getNitcontratista());
        txtNombreContratista.setValue(selectContratista.getNombrecontratista());
        cmbEstadoContratista.setValue(selectContratista.getEstadocontratista());
    }
    
    /** Metodo para actualizar los datos de un contratista
        Se crea un objeto tipo contratista con los datos tomados de los campos del formulario
        y se ejecuta el metodo de la clase ContratistaLogica actualizarContratista**/
    public void modificarContratista() {
        try {
            Contratista updateContratista = selectContratista;
            updateContratista.setNitcontratista(Long.parseLong(txtNitContratista.getValue().toString()));
            updateContratista.setNombrecontratista(txtNombreContratista.getValue().toString());
            updateContratista.setEstadocontratista(cmbEstadoContratista.getValue().toString());
            contratistaLogica.actualizarContratista(updateContratista);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "mensaje", "El Contratista ha sido modificado"));
            limpiarCampos();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "mensaje", ex.getMessage()));
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    /** Metodo para eliminar un contratista
        Se crea un objeto tipo contratista con los datos tomados de los campos del formulario
        y se ejecuta el metodo de la clase ContratistaLogica eliminarContratista**/
    public void eliminarContratista() {
        try {
            Contratista deleteContratista = selectContratista;
            contratistaLogica.eliminarContratista(deleteContratista);
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "mensaje", "El Contratista ha sido eliminado"));
            limpiarCampos();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "mensaje", ex.getMessage()));
        }
    }
    
    /** Metodo para obtener el archivo .xls con los datos de contratistas**/
    public void handleFileUpload(FileUploadEvent event) {
        ServletContext servletContext = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        String rutaDestino = (String) servletContext.getRealPath("/excel"); // Sustituye "/" por el directorio ej: "/upload"

        System.out.println("Ruta Server: " + rutaDestino);
        try {
            copyFile(rutaDestino, event.getFile().getFileName(), event.getFile().getInputstream());
            String resultado = contratistaLogica.importarDatosContratistas(rutaDestino + "\\" + event.getFile().getFileName());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Success: ", resultado));

        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception ex) {
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Error: ", ex.getMessage()));
        }
    }
    
    /** Metodo para copiar el archivo .xls a una carpeta dentro del proyecto  **/
    public void copyFile(String rutaDestino, String fileName, InputStream in) {
        try {
            OutputStream out = new FileOutputStream(new File(rutaDestino + "\\" + fileName));
            System.out.println("Ruta Archivo: " + rutaDestino + "\\" + fileName);
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
            //System.out.println("New file created!");
        } catch (IOException e) {
            //System.out.println(e.getMessage());
        }
    }

    public void generarReporte() {
        try {
            Connection conn = null;
            Context initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:app/jdbc/competencias_tec");
            conn = ds.getConnection();
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("reportes/ReporteBasico.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), null, conn);
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.addHeader("Content-disposition", "attachment; filename=reporteContratistas.pdf");
            ServletOutputStream stream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (NamingException | SQLException | JRException | IOException ex) {
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void generarReporteParametros() {
        try {
            Connection conn = null;
            Context initContext = new InitialContext();
            DataSource ds = (DataSource) initContext.lookup("java:app/jdbc/competencias_tec");
            conn = ds.getConnection();
            Long documento = Long.parseLong(txtNitContratista.getValue().toString());
            Contratista objc = contratistaLogica.consultarxNit(documento);
            Map<String, Object> parametros = new HashMap<>();
            parametros.put("cedula", objc.getCodigocontratista()+"");
            parametros.put("nombre", objc.getNombrecontratista());
            parametros.put("estado", objc.getEstadocontratista());
            File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("reportes/reporteIngresosEmpleado.jasper"));
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(), parametros, conn);
            HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
            response.addHeader("Content-disposition", "attachment; filename=reporteIngresosEmpleado" + documento + ".pdf");
            ServletOutputStream stream = response.getOutputStream();
            JasperExportManager.exportReportToPdfStream(jasperPrint, stream);
            FacesContext.getCurrentInstance().responseComplete();
        } catch (NamingException ex) {
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JRException ex) {
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ContratistaVista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
